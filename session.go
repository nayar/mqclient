package mqclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/nayar/pubsub"
)

type MqError struct {
	ShortDesc string
	LongDesc  string
	Temporary bool
}

func (e *MqError) Error() string {
	return fmt.Sprintf("%s : %s", e.ShortDesc, e.LongDesc)
}

type Session struct {
	sync.Mutex
	url           *url.URL
	user          string
	key           string
	sesId         string
	logged        bool
	retryInterval time.Duration
	httpTimeout   time.Duration
	pullTimeout   time.Duration
	pullMaxItems  int
	pushMaxItems  int
	pushNbatch    int
	pushParallel  int
	pullNitem     int
	expire        int64
	qlimit        int
	plimit        []string
	tags          map[string]string
	ps            *pubsub.Pubsub
	pushQueue     *pubsub.Subscriber
	startId       int64
	prefix        string
	transport     *http.Transport
	client        *http.Client
}

func NewSession(hostUrl string) *Session {
	ses := new(Session)
	u, e := url.Parse(hostUrl)
	if e != nil {
		panic(e.Error())
	}
	ses.url = u
	ses.httpTimeout = 65 * time.Second
	ses.pullTimeout = 60 * time.Second
	ses.pullMaxItems = 100
	ses.pushMaxItems = 100
	ses.retryInterval = 5 * time.Second
	ses.ps = pubsub.DefaultInstance()
	ses.prefix = "r._ps." + pubsub.GetUidStr() + "."
	ses.transport = &http.Transport{Proxy: http.ProxyFromEnvironment, ResponseHeaderTimeout: time.Second * ses.httpTimeout}
	ses.client = &http.Client{Transport: ses.transport}
	return ses
}

func (ses *Session) SetPs(ps *pubsub.Pubsub) *Session {
	ses.ps = ps
	return ses
}

func (ses *Session) Ps() *pubsub.Pubsub {
	return ses.ps
}

func (ses *Session) SetPrefix(prefix string) *Session {
	ses.prefix = prefix
	if !strings.HasSuffix(ses.prefix, ".") {
		ses.prefix += "."
	}
	return ses
}

func (ses *Session) Prefix() string {
	return ses.prefix
}

func (ses *Session) SetRetryInterval(t time.Duration) *Session {
	ses.retryInterval = t
	return ses
}

func (ses *Session) RetryInterval() time.Duration { return ses.retryInterval }

func (ses *Session) SetHttpTimeout(t time.Duration) *Session {
	if t > 0 {
		ses.httpTimeout = t
	} else {
		panic(&MqError{"local_error_range", "Invalid HTTP timeout duration (n > 0)", false})
	}
	ses.transport.ResponseHeaderTimeout = time.Second * ses.httpTimeout
	return ses
}

func (ses *Session) HttpTimeout() time.Duration { return ses.httpTimeout }

func (ses *Session) SetPullTimeout(t time.Duration) *Session {
	if t > 0 {
		ses.pullTimeout = t
	} else {
		panic(&MqError{"local_error_range", "Invalid pull timeout duration, (n > 0)", false})
	}
	return ses
}

func (ses *Session) PullTimeout() time.Duration { return ses.pullTimeout }

func (ses *Session) SetPullMaxItems(n int) *Session {
	if n > 0 {
		ses.pullMaxItems = n
	} else {
		panic(&MqError{"local_error_range", "Invalid max pull items, (n > 0)", false})
	}
	return ses
}

func (ses *Session) PullMaxItems() int { return ses.pullMaxItems }

func (ses *Session) SetPushMaxItems(n int) *Session {
	if n > 0 {
		ses.pushMaxItems = n
	} else {
		panic(&MqError{"local_error_range", "Invalid max push items, (n > 0)", false})
	}
	return ses
}

func (ses *Session) PushMaxItems() int { return ses.pushMaxItems }

func (ses *Session) SetPushParallel(n int) *Session {
	if n >= 0 {
		ses.pushParallel = n
	} else {
		panic(&MqError{"local_error_range", "Invalid push parallel value, (n >= 0)", false})
	}
	return ses
}

func (ses *Session) PushParallel() int { return ses.pushParallel }

func (ses *Session) SetExpire(seconds int64) *Session {
	if seconds >= 0 {
		ses.expire = seconds
	} else {
		panic(&MqError{"local_error_range", "Invalid expire time, (n >= 0)", false})
	}
	return ses
}

func (ses *Session) Expire() int64 { return ses.expire }

func (ses *Session) SetQlimit(n int) *Session {
	if n >= 10 {
		ses.qlimit = n
	} else {
		panic(&MqError{"local_error_range", "Invalid queue limit, (n >= 10)", false})
	}
	return ses
}

func (ses *Session) Qlimit() int { return ses.qlimit }

func (ses *Session) SetPlimit(limits []string) *Session {
	ses.plimit = limits
	return ses
}

func (ses *Session) Plimit() []string { return ses.plimit }

func (ses *Session) SetTags(tags map[string]string) *Session {
	ses.tags = tags
	return ses
}

func (ses *Session) Tags() map[string]string { return ses.tags }

func (ses *Session) Key() string { return ses.key }

func (ses *Session) SesId() string { return ses.sesId }

func (ses *Session) send(p map[string]interface{}) (map[string]interface{}, error) {
	var err error
	var ok bool
	var body []byte
	var raw interface{}
	var rmap map[string]interface{}
	var resp *http.Response
	body, err = json.Marshal(p)
	if err != nil {
		return nil, &MqError{"local_error_marshall", err.Error(), false}
	}
	resp, err = ses.client.Post(ses.url.String(), "application/json", bytes.NewReader(body))
	if err != nil {
		return nil, &MqError{"local_error_http", err.Error(), true}
	}
	if resp.StatusCode != 200 {
		return nil, &MqError{"local_error_http", fmt.Sprintf("HTTP status code: %s", resp.Status), true}
	}
	if _, ok = resp.Header["Content-Type"]; !ok {
		return nil, &MqError{"local_error_contentType", "Missing Content-Type header", true}
	}
	if resp.Header["Content-Type"][0] != "application/json" {
		return nil, &MqError{"local_error_contentType", fmt.Sprintf("Invalid content type: %s", resp.Header["Content-Type"][0]), true}
	}
	var data []byte
	data, err = ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	err = json.Unmarshal(data, &raw)
	if err != nil {
		return nil, &MqError{"local_error_unmarshall", err.Error(), false}
	}
	if rmap, ok = raw.(map[string]interface{}); !ok {
		return nil, &MqError{"local_error_unmarshall", "Response is not JSON object", false}
	}
	if strings.HasPrefix(rmap["type"].(string), "sys_error_") {
		return nil, &MqError{rmap["type"].(string), rmap["data"].(string), false}
	}
	return rmap, nil
}

func (ses *Session) Login(user, password string) error {
	if ses.logged {
		return &MqError{"local_error_login", "Already logged", false}
	}
	d := make(map[string]interface{})
	d["user"] = user
	d["password"] = password
	if ses.expire != 0 {
		d["expire"] = ses.expire
	}
	if ses.qlimit != 0 {
		d["qlimit"] = ses.qlimit
	}
	if ses.plimit != nil {
		d["plimit"] = ses.plimit
	}
	if ses.tags != nil {
		d["tags"] = ses.tags
	}
	p := map[string]interface{}{"type": "login", "data": d}
	res, err := ses.send(p)
	if err != nil {
		return err
	}
	if res["type"] != "sys_done_login" {
		return &MqError{"local_error_response", fmt.Sprintf("Unknown response type: %s", res["type"]), false}
	}
	data := res["data"].(map[string]interface{})
	ses.user = user
	ses.key = data["key"].(string)
	ses.sesId = data["session"].(string)
	ses.logged = true
	return nil
}

func (ses *Session) LoginKey(key string) error {
	if ses.logged {
		return &MqError{"local_error_login", "Already logged", false}
	}
	d := make(map[string]interface{})
	d["fields"] = []string{"id", "expire", "plimit", "qlimit", "user", "tags", "nitem", "nbatch"}
	p := map[string]interface{}{"type": "whoami", "data": d, "key": key}
	res, err := ses.send(p)
	if err != nil {
		return err
	}

	if res["type"] != "sys_done_whoami" {
		return &MqError{"local_error_response", fmt.Sprintf("Unknown response type: %s", res["type"]), false}
	}
	data := res["data"].(map[string]interface{})
	ses.key = key
	ses.sesId = data["id"].(string)
	ses.user = data["user"].(string)
	ses.expire = int64(data["expire"].(float64))
	ses.qlimit = int(data["qlimit"].(float64))
	ses.plimit = make([]string, 0)
	ses.tags = make(map[string]string)
	ses.pushNbatch = int(data["nbatch"].(float64))
	ses.pullNitem = int(data["nitem"].(float64))
	for _, v := range data["plimit"].([]interface{}) {
		ses.plimit = append(ses.plimit, v.(string))
	}
	for k, v := range data["tags"].(map[string]interface{}) {
		ses.tags[k] = v.(string)
	}
	ses.logged = true
	return nil
}

func (ses *Session) Logout() error {
	ses.Stop()
	if !ses.logged {
		return &MqError{"local_error_login", "Not logged", false}
	}
	ses.logged = false
	p := map[string]interface{}{"type": "logout", "key": ses.key}
	res, err := ses.send(p)
	if err != nil {
		return err
	}
	if res["type"] != "sys_done_logout" {
		return &MqError{"local_error_response", fmt.Sprintf("Unknown response type: %s", res["type"]), false}
	}
	return nil
}

func (ses *Session) Purge() error {
	if !ses.logged {
		return &MqError{"local_error_login", "Not logged", false}
	}
	p := map[string]interface{}{"type": "purge", "key": ses.key}
	res, err := ses.send(p)
	if err != nil {
		return err
	}
	if res["type"] != "sys_done_purge" {
		return &MqError{"local_error_response", fmt.Sprintf("Unknown response type: %s", res["type"]), false}
	}
	return nil
}

func (ses *Session) pushWorker() {
	startId := ses.startId
	for {
		ses.Lock()
		if startId != ses.startId {
			ses.Unlock()
			break
		}
		ses.Unlock()
		var pbuf []interface{}
		for {
			msg, ok := ses.pushQueue.Wait(2 * time.Second)
			if !ok {
				continue
			}
			pbuf = append(pbuf, msg.Val)
			if ses.pushQueue.Waiting() == 0 || len(pbuf) >= ses.pushMaxItems {
				break
			}
		}
		ses.pushNbatch += 1
		p := map[string]interface{}{"type": "push", "key": ses.key, "data": pbuf, "nbatch": ses.pushNbatch}
		if ses.pushParallel > 0 {
			p["parallel"] = ses.pushParallel
		}
		res, err := ses.send(p)
		if err != nil {
			ses.pushQueue.Flush()
			ses.ps.Send(&pubsub.Message{To: ses.prefix + "error", Val: err, Pri: 10})
			time.Sleep(ses.retryInterval)
		} else if res["type"].(string) != "sys_done_push" {
			ses.pushQueue.Flush()
			ses.ps.Send(&pubsub.Message{To: ses.prefix + "error", Val: &MqError{"local_error_push", "Unknown Response type: " + res["type"].(string), false}, Pri: 10})
			time.Sleep(ses.retryInterval)
		}
	}
}

func (ses *Session) pullWorker() {
	startId := ses.startId
	for {
		ses.Lock()
		if startId != ses.startId {
			ses.Unlock()
			break
		}
		ses.Unlock()
		d := map[string]interface{}{"nitem": ses.pullNitem, "timeout": ses.pullTimeout / time.Second, "max": ses.pullMaxItems}
		p := map[string]interface{}{"type": "pull", "key": ses.key, "data": d}
		res, err := ses.send(p)
		if err != nil {
			time.Sleep(ses.retryInterval)
			continue
		}
		if res["type"].(string) == "sys_done_pull" {
			items := res["data"].([]interface{})
			for _, item := range items {
				i := item.(map[string]interface{})
				nitem := int(i["nitem"].(float64))
				iden := int64(i["id"].(float64))
				ses.pullNitem = nitem + 1
				ses.ps.Send(&pubsub.Message{To: ses.prefix + "response." + strconv.FormatInt(iden, 10), Val: i})
			}
		}

	}
}

func (ses *Session) Start() error {
	if !ses.logged {
		return &MqError{"local_error_start", "Not logged", false}
	}

	ses.Lock()
	if ses.startId != 0 {
		ses.Unlock()
		return &MqError{"local_error_start", "Already started", false}
	}
	ses.Unlock()
	ses.pushQueue = ses.ps.NewSubscriber().SetQueueSize(10000)
	ses.pushQueue.Subscribe(ses.prefix + "push")
	ses.startId = pubsub.GetUid()
	go ses.pushWorker()
	go ses.pullWorker()
	return nil
}

func (ses *Session) Stop() error {
	ses.Lock()
	if ses.startId == 0 {
		ses.Unlock()
		return &MqError{"local_error_stop", "Already stopped", false}
	}
	ses.Unlock()
	ses.startId = 0
	ses.pushQueue.Clear()
	ses.ps.Send(&pubsub.Message{To: ses.prefix + "error", Val: &MqError{"local_error_stop", "Client stopped", false}, Pri: 10})
	return nil
}

func (ses *Session) Started() bool {
	ses.Lock()
	started := ses.startId != 0
	ses.Unlock()
	return started
}

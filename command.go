package mqclient

import (
	"strconv"
	"strings"
	"time"

	"bitbucket.org/nayar/pubsub"
)

type Command struct {
	ses *Session
	id  int64
	su  *pubsub.Subscriber
}

func (ses *Session) NewCommand() *Command {
	cmd := new(Command)
	cmd.ses = ses
	cmd.id = pubsub.GetUid()
	cmd.su = ses.ps.NewSubscriber()
	cmd.su.Subscribe(ses.prefix+"response."+strconv.FormatInt(cmd.id, 10), ses.prefix+"error")
	return cmd
}

func (cmd *Command) Send(m map[string]interface{}) {
	m["id"] = cmd.id
	cmd.ses.ps.Send(&pubsub.Message{To: cmd.ses.prefix + "push", Val: m})
}

func (cmd *Command) Wait(t time.Duration) (map[string]interface{}, error) {
	msg, ok := cmd.su.Wait(t)
	if !ok {
		return nil, &MqError{"local_error_timeout", "Response timeout", true}
	}
	if e, ok := msg.Val.(*MqError); ok {
		return nil, e
	}
	rmap := msg.Val.(map[string]interface{})
	if strings.HasPrefix(rmap["type"].(string), "sys_error_") {
		return nil, &MqError{rmap["type"].(string), rmap["data"].(string), false}
	}
	return rmap, nil
}

func (cmd *Command) Waiting() int {
	return cmd.su.Waiting()
}

func (cmd *Command) GetId() int64 {
	return cmd.id
}

package args

import (
	"fmt"
	"strings"
	"strconv"
	"math"
)

const (
	_MaxInt = int(^uint(0) >> 1) 
	_MinInt = -_MaxInt - 1
	_MaxFloat = math.MaxFloat64
	_MinFloat = -_MaxFloat
	_MaxLength = 1000
)

type Argument interface {
	Check(field string, value *interface{}) (error)
}
type Fields map[string]Argument

// Any Type
type AnyArg struct {
	optional bool
	defval interface{}
	values []interface{}
	types []string
}
func Arg() (*AnyArg) {
	arg := &AnyArg{ false, nil, []interface{}{}, []string{} }
	return arg
}
func (arg *AnyArg) Optional(opt bool) (*AnyArg) {
	arg.optional = opt
	return arg
}
func (arg *AnyArg) Default(val interface{}) (*AnyArg) {
	arg.defval = val
	return arg
}
func (arg *AnyArg) Types(types []string) (*AnyArg) {
	arg.types  = types
	return arg
}
func (arg *AnyArg) ValidValues(values ...interface{}) (*AnyArg) {
	arg.values = values
	return arg
}
func (arg *AnyArg) Check(field string, value *interface{}) (error) {
	if *value == nil {
		if arg.defval != nil {
			*value = arg.defval
		} else if arg.optional {
			return nil
		} else {
			return fmt.Errorf("%s is required", field)
		}
	}
	if len(arg.types) != 0 {
		found := false
		for _, t := range arg.types {
			if isType(value, t) {
				found = true
				break
			}
		}
		if !found {
			return fmt.Errorf("%s type must be one of %#v", field, arg.types)
		}
	}
	if len(arg.values) != 0 {
		found := false
		for _, val := range arg.values {
			if *value == val {
				found = true
				break
			}
		}
		if !found {
			return fmt.Errorf("%s value is invalid, must be one of: %v", field, arg.values)
		}
	}
	return nil
}

// Integer
type IntArg struct {
	AnyArg
	min int
	max int
}

func Int() (*IntArg) {
	return &IntArg{*Arg(), _MinInt, _MaxInt}
}
func (arg *IntArg) Optional(opt bool) (*IntArg) {
	arg.optional = opt
	return arg
}
func (arg *IntArg) Default(val int) (*IntArg) {
	arg.defval = val
	return arg
}
func (arg *IntArg) ValidValues(values ...int) (*IntArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *IntArg) Min(min int) (*IntArg) {
	arg.min = min
	return arg
}
func (arg *IntArg) Max(max int) (*IntArg) {
	arg.max = max
	return arg
}
func (arg *IntArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		v, ok := (*value).(int)
		if !ok {
			if err := autoConvert(value, "int"); err != nil {
				return fmt.Errorf("%s must be int instead of %T", field, *value)
			}
			v = (*value).(int)
		}
		if v < arg.min {
			return fmt.Errorf("%s is below minimum value %d", field, arg.min)
		}
		if v > arg.max {
			return fmt.Errorf("%s is above maximum value %d", field, arg.max)
		}
	}
	return nil
}

// Float
type FloatArg struct {
	AnyArg
	min float64
	max float64
	round int
}

func Float() (*FloatArg) {
	return &FloatArg{*Arg(), _MinFloat, _MaxFloat, -1}
}
func (arg *FloatArg) Optional(opt bool) (*FloatArg) {
	arg.optional = opt
	return arg
}
func (arg *FloatArg) Default(val float64) (*FloatArg) {
	arg.defval = val
	return arg
}
func (arg *FloatArg) ValidValues(values ...float64) (*FloatArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *FloatArg) Min(min float64) (*FloatArg) {
	arg.min = min
	return arg
}
func (arg *FloatArg) Max(max float64) (*FloatArg) {
	arg.max = max
	return arg
}
func (arg *FloatArg) Round(n int) (*FloatArg) {
	arg.round = n
	return arg
}
func (arg *FloatArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		v, ok := (*value).(float64)
		if !ok {
			if err := autoConvert(value, "float"); err != nil {
				return fmt.Errorf("%s must be float instead of %T", field, *value)
			}
			v = (*value).(float64)
		}
		if v < arg.min {
			return fmt.Errorf("%s is below minimum value %f", field, arg.min)
		}
		if v > arg.max {
			return fmt.Errorf("%s is above maximum value %f", field, arg.max)
		}
		if arg.round >= 0 {
			var r float64
			pow := math.Pow(10, float64(arg.round))
			digit := pow * v
			_, div := math.Modf(digit)
			if div >= 0.5 {
				r = math.Ceil(digit)
			} else {
				r = math.Floor(digit)
			}
			*value = r / pow
		}
	}
	return nil
}

// String
type StringArg struct {
	AnyArg
	minlen int
	maxlen int
	allowed string
	upper bool
	lower bool
}
func String() (*StringArg) {
	return &StringArg{*Arg(), 0, _MaxInt, "", false, false}
}
func (arg *StringArg) Optional(opt bool) (*StringArg) {
	arg.optional = opt
	return arg
}
func (arg *StringArg) Default(val string) (*StringArg) {
	arg.defval = val
	return arg
}
func (arg *StringArg) ValidValues(values ...string) (*StringArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *StringArg) MaxLen(maxlen int) (*StringArg) {
	arg.maxlen = maxlen
	return arg
}
func (arg *StringArg) MinLen(minlen int) (*StringArg) {
	arg.minlen = minlen
	return arg
}
func (arg *StringArg) Allow(allowed ...string) (*StringArg) {
	allow := ""
	for _, a := range allowed {
		if a == "digits" {
			allow += "0123456789"
		} else if a == "letters" {
			allow += "abcdefghijklmnopqrstuvwxyz"
			allow += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		} else if a == "lower" {
			allow += "abcdefghijklmnopqrstuvwxyz"
		} else if a == "upper" {
			allow += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		} else {
			allow += a
		}
	}
	arg.allowed = allow
	return arg
}
func (arg *StringArg) ToUpper() (*StringArg) {
	arg.upper = true
	return arg
}
func (arg *StringArg) ToLower() (*StringArg) {
	arg.lower = true
	return arg
}
func (arg *StringArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		v, ok := (*value).(string)
		if !ok {
			if err := autoConvert(value, "string"); err != nil {
				return fmt.Errorf("%s must be string instead of %T", field, *value)
			}
			v = (*value).(string)
		}
		if len(v) > arg.maxlen {
			return fmt.Errorf("%s excedes max length (%d)", field, arg.maxlen)
		}
		if len(v) < arg.minlen {
			return fmt.Errorf("%s excedes min length (%d)", field, arg.minlen)
		}
		if arg.upper {
			*value = strings.ToUpper(v)
			v = (*value).(string)
		}
		if arg.lower {
			*value = strings.ToLower(v)
			v = (*value).(string)
		}
		if arg.allowed != "" {
			for _, c := range v {
				if !strings.ContainsRune(arg.allowed, c) {
					return fmt.Errorf("%s contains invalid character (%c)", field, c)
				}
			}
		}
	}
	return nil
}

// Bool
type BoolArg struct {
	AnyArg
}
func Bool() (*BoolArg) {
	return &BoolArg{*Arg()}
}
func (arg *BoolArg) Optional(opt bool) (*BoolArg) {
	arg.optional = opt
	return arg
}
func (arg *BoolArg) Default(val bool) (*BoolArg) {
	arg.defval = val
	return arg
}
func (arg *BoolArg) ValidValues(values ...bool) (*BoolArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *BoolArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		_, ok := (*value).(bool)
		if !ok {
			if err := autoConvert(value, "bool"); err != nil {
				return fmt.Errorf("%s must be bool instead of %T", field, *value)
			}
		}
	}
	return nil
}

// Map
type MapArg struct {
	AnyArg
	fields Fields
	nomore bool
	minlen int
	maxlen int
}
func Map(fields Fields) (*MapArg) {
	return &MapArg{*Arg(), fields, false, 0, _MaxLength}
}
func (arg *MapArg) Optional(opt bool) (*MapArg) {
	arg.optional = opt
	return arg
}
func (arg *MapArg) NoMoreFields() (*MapArg) {
	arg.nomore = true
	return arg
}
func (arg *MapArg) Default(val map[string]interface{}) (*MapArg) {
	arg.defval = val
	return arg
}
func (arg *MapArg) ValidValues(values ...map[string]interface{}) (*MapArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *MapArg) MaxLen(maxlen int) (*MapArg) {
	arg.maxlen = maxlen
	return arg
}
func (arg *MapArg) MinLen(minlen int) (*MapArg) {
	arg.minlen = minlen
	return arg
}
func (arg *MapArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		v, ok := (*value).(map[string]interface{})
		if !ok {
			if err := autoConvert(value, "map"); err != nil {
				return fmt.Errorf("%s must be map instead of %T", field, *value)
			}
			v = (*value).(map[string]interface{})
		}
		if len(v) > arg.maxlen {
			return fmt.Errorf("%s excedes max length (%d)", field, arg.maxlen)
		}
		if len(v) < arg.minlen {
			return fmt.Errorf("%s excedes min length (%d)", field, arg.minlen)
		}
		for argf, argv := range arg.fields {
			var d interface{}
			d, _ = v[argf]
			if err := argv.Check(field+"["+argf+"]", &d); err != nil {
				return err
			
			} else if d != nil {
				v[argf] = d
			}
		}
		if arg.nomore {
			for dataf, _ := range v {
				if _, ok := arg.fields[dataf]; !ok {
					return fmt.Errorf("%s[%s] is not an allowed field", field, dataf)
				} 
			}
		}
	}
	return nil
}

// List
type ListArg struct {
	AnyArg
	elements Argument
	minlen int
	maxlen int
}
func List() (*ListArg) {
	return &ListArg{*Arg(), nil, 0, _MaxLength}
}
func (arg *ListArg) Elements(elems Argument) (*ListArg) {
	arg.elements = elems
	return arg
}
func (arg *ListArg) Optional(opt bool) (*ListArg) {
	arg.optional = opt
	return arg
}
func (arg *ListArg) Default(val []interface{}) (*ListArg) {
	arg.defval = val
	return arg
}
func (arg *ListArg) ValidValues(values ...[]interface{}) (*ListArg) {
	arg.values = make([]interface{}, len(values))
	for i, v := range values {
		arg.values[i] = v
	}
	return arg
}
func (arg *ListArg) MaxLen(maxlen int) (*ListArg) {
	arg.maxlen = maxlen
	return arg
}
func (arg *ListArg) MinLen(minlen int) (*ListArg) {
	arg.minlen = minlen
	return arg
}
func (arg *ListArg) Check(field string, value *interface{}) (error) {
	if err := arg.AnyArg.Check(field, value); err != nil {
		return err
	}
	if *value != nil {
		v, ok := (*value).([]interface{})
		if !ok {
			if err := autoConvert(value, "list"); err != nil {
				return fmt.Errorf("%s must be list instead of %T", field, *value)
			}
			v = (*value).([]interface{})
		}
		if len(v) > arg.maxlen {
			return fmt.Errorf("%s excedes max length (%d)", field, arg.maxlen)
		}
		if len(v) < arg.minlen {
			return fmt.Errorf("%s excedes min length (%d)", field, arg.minlen)
		}
		if arg.elements != nil {
			for i, _ := range v {
				d := &v[i]
				if err := arg.elements.Check(fmt.Sprintf("%s[%d]",field,i), d); err != nil {
					return err
				}
			}
		}		
	}
	return nil
}

// Any
type AnyOp struct {
	args []Argument
}
func Any(args ...Argument) (*AnyOp) {
	return &AnyOp{args}
}
func (arg *AnyOp) Check(field string, value *interface{}) (error) {
	errs := []string{}
	if len(arg.args) <= 0 {
		return nil
	}
	origval := *value
	for _, a := range arg.args {
		if err := a.Check(field, value); err == nil {
			return nil
		} else {
			*value = origval
			errs = append(errs, err.Error())
		}
	}
	return fmt.Errorf("%s was incorrect in all of its forms: [%s]", field, strings.Join(errs, ", "))
}

// Pipe
type PipeOp struct {
	args []Argument
}
func Pipe(args ...Argument) (*PipeOp) {
	return &PipeOp{args}
}
func All(args ...Argument) (*PipeOp) {
	return &PipeOp{args}
}
func (arg *PipeOp) Check(field string, value *interface{}) (error) {
	for _, a := range arg.args {
		if err := a.Check(field, value); err != nil {
			return fmt.Errorf("Error checking piped field: %s", err.Error())
		}
	}
	return nil
}

// Type conversion and control
func convertStrInt(d *interface{}) {
	if data, err1 := strconv.ParseInt((*d).(string), 10, 0); err1 != nil {
		if dataf, err2 := strconv.ParseFloat((*d).(string), 64); err2 != nil {
			panic(err1)
		} else {
			var aux interface{} = float64(dataf)
			convertFloatInt(&aux)
			*d = int(aux.(int))
		}
	} else {
		*d = int(data)
	}
}
func convertStrFloat(d *interface{}) {
	if data, err := strconv.ParseFloat((*d).(string), 64); err != nil {
		panic(err)
	} else {
		*d = data
	}
}
func convertStrBool(d *interface{}) {
	for _, trueVal := range []string{"true", "True", "1"} {
		if trueVal == (*d).(string) {
			*d = true
		}
	}
	*d = false
}
func convertIntStr(d *interface{}) {
	*d = strconv.FormatInt(int64((*d).(int)), 10)
}
func convertIntFloat(d *interface{}) {
	*d = float64((*d).(int))
}
func convertIntBool(d *interface{}) {
	*d = (*d).(int) > 0
}
func convertFloatStr(d *interface{}) {
	*d = strconv.FormatFloat((*d).(float64), 'f', -1, 64)
}
func convertFloatInt(d *interface{}) {
	*d = int((*d).(float64))
}
func convertFloatBool(d *interface{}) {
	*d = (*d).(float64) > 0.0
}
func convertBoolInt(d *interface{}) {
	if (*d).(bool) {
		*d = 1
	} else {
		*d = 0
	}
}
func convertBoolFloat(d *interface{}) {
	if (*d).(bool) {
		*d = 1.0
	} else {
		*d = 0.0
	}
}
func convertBoolStr(d *interface{}) {
	*d = strconv.FormatBool((*d).(bool))
}
func convertNone(d *interface{}) {
}
func autoConvert(d *interface{}, to string) (err error) {
	switch (*d).(type) {
		case int:
			return convert(d, "int", to)
		case float64:
			return convert(d, "float", to)
		case float32:
			*d = float64((*d).(float32))
			return convert(d, "float", to)
		case bool:
			return convert(d, "bool", to)
		case string:
			return convert(d, "string", to)
		default:
			return fmt.Errorf("auto-conversion of %#v to %s not possible", *d, to)
	}
}
func convert(d *interface{}, from string, to string) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("conversion of %#v from %s to %s failed: %v", *d, from, to, r)
		}
	}()
	if convF, ok := conversions[from][to]; !ok || convF == nil {
		return fmt.Errorf("conversion of %#v from %s to %s not possible", *d, from, to)
	} else {
		convF(d)
	}
	return err
}
var conversions map[string]map[string]func (d *interface{}) = map[string]map[string]func (d *interface{}) {
	"string": { "string": convertNone, "int": convertStrInt, "float": convertStrFloat, "bool": convertStrBool, "map": nil, "list": nil, "": nil, },
	"int": { "string": convertIntStr, "int": convertNone, "float": convertIntFloat, "bool": convertIntBool, "map": nil, "list": nil, "": nil, },
	"float": { "string": convertFloatStr, "int": convertFloatInt, "float": convertNone, "bool": convertFloatBool, "map": nil, "list": nil, "": nil, },
	"bool": { "string": convertBoolStr, "int": convertBoolInt, "float": convertBoolFloat, "bool": convertNone, "map": nil, "list": nil, "": nil, },
	"map": { "string": nil, "int": nil, "float": nil, "bool": nil, "map": convertNone, "list": nil, "": nil, },
	"list": { "string": nil, "int": nil, "float": nil, "bool": nil, "map": nil, "list": convertNone, "": nil, },
	"": { "string": nil, "int": nil, "float": nil, "bool": nil, "map": nil, "list": nil, "": nil, },
}
func isType(d *interface{}, t string) bool {
	var ok bool
	switch t {
	case "string":
		_, ok = (*d).(string)
		return ok
	case "int":
		_, ok = (*d).(int)
		return ok
	case "float":
		_, ok = (*d).(float64)
		if ok {
			return true
		}
		_, ok = (*d).(float32)
		if ok {
			return true
		}
		return false
	case "bool":
		_, ok := (*d).(bool)
		return ok
	case "map":
		_, ok := (*d).(map[string]interface{})
		return ok
	case "list":
		_, ok := (*d).([]interface{})
		return ok
	default:
		return false
	}
}
func getType(arg Argument) string {
	switch arg.(type) {
		case *StringArg:
			return "string"
		case *IntArg:
			return "int"
		case *FloatArg:
			return "float"
		case *BoolArg:
			return "bool"
		case *MapArg:
			return "map"
		case *ListArg:
			return "list"
		default:
			return ""
	}
}
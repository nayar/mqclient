package mqapi

import (
	"bitbucket.org/nayar/mqclient"
	. "bitbucket.org/nayar/mqclient/mqapi/args"
	"bitbucket.org/nayar/n4m/ini"
	_ "net"
	_ "net/http"
	_ "net/url"
	"time"
	"runtime"
	"strings"
	"errors"
	"strconv"
	"log"
	"fmt"
)

var logDateFormat = "02/01/2006 15:04:05"

type MqApi struct {
	session *mqclient.Session
	
	mqUrl string
	mqPath string
	mqUser string
	mqPass string
	mqUsersPath string
	mqQueue string
	mqPerm string
	mqPermsPrefix string
	
	methods map[string]*Method

	users map[string]*User
	usersAuth bool
	usersLock chan bool
	usersExpire int
	permsExpire int

	roles map[string]*Role
	roleNames []string
	hasRoles bool
	rolesCheckCache map[string]map[string]bool
	
	objChecks []string
	permChecks []string
	cmdChecks []map[string]interface{}

	cmdWait int
	taskWait int
	errNum int
	recreateAfter int
	onPanic string
	stopChan chan bool
	lock chan bool
}

type Method struct {
	function func(map[string]interface{}, *User) (interface{}, bool)
	argument Argument
	roles map[string]bool
}

// Spawns new API
func New(conff string) (*MqApi, error) {
	api := new(MqApi)
	if conff == "" {
		conff = "mqapi.ini"
	}
	sections, err := ini.LoadFile(conff)
	if err != nil {
		return nil, errors.New("error opening config file [" + err.Error() + "]")
	}
	if ms, ok := sections["mqapi"]; ok {
		if api.mqUrl, ok = ms["url"]; !ok {
			api.mqUrl = "https://mqnodes.mqspace.com/mqspace/ws"
		}
		if api.mqPath, ok = ms["path"]; !ok {
			api.mqPath = "root"
		}
		if api.mqUser, ok = ms["user"]; !ok {
			return nil, errors.New("Error: Missing user.")
		}
		if api.mqPass, ok = ms["password"]; !ok {
			return nil, errors.New("Error: Missing password.")
		}
		if api.mqPerm, ok = ms["permission"]; !ok {
			return nil, errors.New("Error: Missing permission.")
		}
		if api.mqPermsPrefix, ok = ms["permissions_prefix"]; !ok {
			return nil, errors.New("Error: Missing permissions prefix.")
		}
		if api.onPanic, ok = ms["on_panic"]; !ok {
			api.onPanic = "print"
		} else if api.onPanic != "logout" && api.onPanic != "print" {
			return nil, errors.New("Error: onpanic must be 'logout' or 'print'")
		}
		cw, _ := ms["cmd_wait"]
		api.cmdWait, err = strconv.Atoi(cw)
		if err != nil {
			api.cmdWait = 20
		}
		tw, _ := ms["task_wait"]
		api.taskWait, err = strconv.Atoi(tw)
		if err != nil {
			api.taskWait = 600
		}
		ra, _ := ms["recreate_after"]
		api.recreateAfter, err = strconv.Atoi(ra)
		if err != nil {
			api.recreateAfter = 10
		}
		ua, ok := ms["users_auth"]
		if !ok {
			api.usersAuth = true
		} else if ua == "no" {
			api.usersAuth = false
		} else {
			api.usersAuth = true
		}
		api.usersLock = make(chan bool, 1)
		ue, _ := ms["users_expire"]
		api.usersExpire, err = strconv.Atoi(ue)
		if err != nil {
			api.usersExpire = 300
		}
		pe, _ := ms["permissions_expire"]
		api.permsExpire, err = strconv.Atoi(pe)
		if err != nil {
			api.permsExpire = 120
		}
	} else {
		return nil, errors.New("mqapi section not found")
	}
	api.mqUsersPath = api.mqPath + "/users"
	api.mqQueue = api.mqPath + "/api"
	api.mqUser = api.mqUsersPath + "/api_worker"
	api.stopChan = make(chan bool, 0)
	api.lock = make(chan bool, 1)
	api.lock <- true
	api.methods = map[string]*Method{}
	api.users = map[string]*User{}
	api.roleNames = []string{}
	api.roles = map[string]*Role{}
	api.rolesCheckCache = map[string]map[string]bool{}
	if api.usersAuth {
		api.usersAuthRoutine()
	}
	return api, nil
}

// CONFIGURATION
func (api *MqApi) MQPath() string {
	return api.mqPath
}
func (api *MqApi) MQUsersPath() string {
	return api.mqUsersPath
}
func (api *MqApi) MQQueue() string {
	return api.mqQueue
}
func (api *MqApi) MQPermsPrefix() string {
	return api.mqPermsPrefix
}
func (api *MqApi) MQPerm() string {
	return api.mqPerm
}

// METHODS
func (api *MqApi) SetMethod(name string, roles map[string]bool, args Argument, f func(args map[string]interface{}, user *User) (interface{}, bool)) {
	api.methods[name] = &Method{f, args, roles}
}

// MQSPACE CHECKING
func (api *MqApi) AddObjectCheck(obj, cls string) {
	api.objChecks = append(api.objChecks, obj+":"+cls)
}
func (api *MqApi) AddPermCheck(path, perm string) {
	api.permChecks = append(api.permChecks, path+":"+perm)
}
func (api *MqApi) AddOwnerCheck(path string) {
	api.AddPermCheck(path, "owner")
}
func (api *MqApi) AddCommandCheck(cmd map[string]interface{}) {
	api.cmdChecks = append(api.cmdChecks, cmd)
}

// API
func (api *MqApi) Start() {
	select {
		case <- api.lock:
			defer func(api *MqApi) {
				api.lock <- true
			}(api)
			log.Print("> START")
			<- api.usersLock
			api.users = map[string]*User{}
			api.usersLock <- true
			if api.onPanic == "print" {
				defer printErrOnPanic()
			} else {
				defer logoutOnPanic(api.session)
			}
			if !api.startSession() {
				log.Println("ERROR: Session couldn't be started.")
				return
			}
			if !api.check() {
				return
			}
			log.Println("MQApi has been started")
			api.serve()
		default:
	}
}

func (api *MqApi) Stop() {
	log.Print("> STOP")
	api.stopChan <- true
	<- api.stopChan
}

// Serving routine, pulls correct tasks and recreates session if it seems erroneous
func (api *MqApi) serve() {
	go func() {
		defer stopOnPanic(api)
		api.errNum = 0
		for {
			// Check number of consecutive errors for session
			if api.errNum == api.recreateAfter {
				log.Println("> Reached max number of consecutive errors. Re-creating session.")
				api.startSession()
				api.errNum = 0
			}
			// Create command
			tp := api.session.NewCommand()
			// Pull the queue
			tp.Send(map[string]interface{}{"type": "task_pull", "data": map[string]interface{}{"path": api.mqQueue, "flags": ""}})
			// Wait for a task
			r, err := tp.Wait(time.Duration(api.taskWait) * time.Second)
			if err != nil {
				errs := fmt.Sprintf("%d seconds without a request...", api.taskWait)
				log.Println(errs)
				tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": errs}})
				api.errNum += 1
				continue
			}
			// Receive task_push
			if r["type"].(string) != "task_push" {
				log.Println("Discarding request, not task_push")
				tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Discarding request, not task_push"}})
				continue
			}
			api.errNum = 0
			go api.processTask(tp, r)
		}
	}()
	<- api.stopChan
	api.stopSession()
	log.Println("MQApi has been stopped")
	api.stopChan <- true
}

// Processes a correct task: checks data, user auth, permissions... It calls API function or returns error.
func (api *MqApi) processTask(tp *mqclient.Command, r map[string]interface{}) {
	if api.onPanic == "print" {
		defer printErrOnPanic()
	} else {
		defer logoutOnPanic(api.session)
	}
	
	var err error
	d := r["data"].(map[string]interface{})

	// Check data is map
	mapd, ok := d["data"].(map[string]interface{})
	if !ok {
		log.Println("Discarding request, task data must be of type [map]")
		tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Task data must be of type [map]"}})
		return
	}
	// Check func field
	f, ok := mapd["func"].(string)
	if !ok {
		log.Println("Discarding request, data must have a [func] field of type [string]")
		tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Data must have a [func] field of type [string]"}})
		return
	}
	// Check args field
	args, ok := mapd["args"].(map[string]interface{})
	if !ok {
		log.Println("Discarding request, data must have an [args] field of type [map]")
		tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Data must have an [args] field of type [map]"}})
		return
	}
	
	usr := d["sender_user"].(string)

	user := &User{}
	if api.usersAuth {
		if user, err = api.AuthUser(usr); err != nil {
			log.Println("Discarding request, error with user:", err)
			tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Error with user: "+err.Error()}})
			return
		}
	}

	// Check api function exist
	fun, ok := api.methods[f]
	if !ok {
		log.Println("Discarding request, missing func ["+f+"]")
		tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Missing func [" + f + "]"}})
		return
	}
	
	// Check permissions
	if api.hasRoles {
		if !user.role.In(fun.roles) {
			log.Println("Discarding request, user role [", user.role.Name(), "] not valid: must be one of [", formatRoles(fun.roles), "]")
			tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": fmt.Sprintf("Error with user role [%s]: must be one of [%s]", user.role.Name(), formatRoles(fun.roles))}})
			return
		}
	}

	// Check api function arguments
	var argsdata interface{} = args
	if err = fun.argument.Check("args", &argsdata); err != nil {
		log.Println("Discarding request, error with args:", err.Error())
		tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": true, "result": "Error with args: " + err.Error()}})
		return
	}

	// Call API function
	res, hasErr := fun.function(args, user)
	if e, ok := res.(error); ok {
		res = e.Error()
	}
	tp.Send(map[string]interface{}{"type": "task_end", "data": map[string]interface{}{"error": hasErr, "result": res}})
} 


// Utils

// Re/Starts the session
func (api *MqApi) startSession() bool {
	if api.session != nil && (api.session.Key() != "" || api.session.Started()) {
		api.stopSession()
	}
	log.Printf("Creating worker session...")
	s := mqclient.NewSession(api.mqUrl)
	if err := s.Login(api.mqUser, api.mqPass); err != nil {
		return false
	}
	if err := s.Start(); err != nil {
		return false
	}
	api.session = s
	return true
}

// Stops the session
func (api *MqApi) stopSession() {
	log.Print("Closing worker session...")
	api.session.Logout()
	api.session = nil
}

// Do all the API checking
func (api *MqApi) check() bool {
	// Check Objects
	log.Print("Checking needed objects...")
	for _, oc := range api.objChecks {
		spl := strings.Split(oc, ":")
		data := map[string]interface{}{
			"type": "object_search",
			"data": map[string]interface{}{ "path": spl[0], "class": spl[1] },
		}
		if r, err := api.MQSend(data); err != nil {
			log.Println("ERROR: ", err)
			return false
		} else {
			if r["type"] != "sys_done_object_search" || len(r["data"].([]interface{})) != 1 {
				log.Printf("ERROR: object [%s] of class [%s] does not exist\n", spl[0], spl[1])
				return false
			}
		}
	}
	// Check Permissions
	log.Print("Checking needed permissions...")
	for _, pc := range api.permChecks {
		spl := strings.Split(pc, ":")
		data := map[string]interface{}{
			"type": "permission_check",
			"data": map[string]interface{}{ "path": spl[0], "perm": spl[1], "ses": api.session.SesId() },
		}
		if r, err := api.MQSend(data); err != nil {
			log.Println("ERROR: ", err)
			return false
		} else {
			if r["type"] == "sys_done_permission_check" {
				if d, ok := r["data"].(map[string]interface{}); ok {
					if v, ok := d["valid"].(bool); !ok || !v {
						log.Printf("ERROR: user [%s] has not [%s] permission on path [%s]\n", api.mqUser, spl[1], spl[0])
						return false
					}
				}
			}
		}
	}
	// Check Commands
	log.Print("Running check commands...")
	for _, c := range api.cmdChecks {
		if _, err := api.MQSend(c); err != nil {
			log.Printf("ERROR running command %s: %s\n", c, err)
			return false
		}
	}
	return true
}

// Send a command to mqspace and wait for response
func (api *MqApi) MQSend(d map[string]interface{}) (map[string]interface{}, error) {
	return api.MQSendAndWait(d, api.cmdWait)
}
func (api *MqApi) MQSendAndWait(d map[string]interface{}, seconds int) (map[string]interface{}, error) {
	cmd := api.session.NewCommand()
	cmd.Send(d)
	r, err := cmd.Wait(time.Duration(seconds) * time.Second)
	if err != nil {
		return nil, err
	}
	return r, nil
}

// Defer function for recovering from panic, logging out, and panicking again
func logoutOnPanic(session *mqclient.Session) {
	if r := recover(); r != nil {
		log.Print("Worker panic. Logging out... ")
		session.Logout()
		panic(r)
	}
}

// Defer function for stopping worker on panic.
func stopOnPanic(api *MqApi) {
	if r := recover(); r != nil {
		log.Print("Worker panic. Stop worker...")
		api.Stop()
	}
}

// Defer function for recovering from panic and printing the error
func printErrOnPanic() {
	if r := recover(); r != nil {
		log.Println("Worker panic: ", r)
		log.Println("")

		var name, file string
		var line int
		var pc [16]uintptr

		n := runtime.Callers(3, pc[:])
		for _, pc := range pc[:n] {
			fn := runtime.FuncForPC(pc)
			if fn == nil {
				continue
			}
			file, line = fn.FileLine(pc)
			name = fn.Name()
			if !strings.HasPrefix(name, "runtime.") {
				break
			}
		}

		switch {
		case name != "":
			log.Printf("%v:%v", name, line)
			return
		case file != "":
			log.Printf("%v:%v", file, line)
			return
		}

		log.Printf("pc:%x", pc)
	}
}


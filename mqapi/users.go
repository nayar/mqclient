package mqapi

import (
	"fmt"
	"time"
	"strings"
)

// USERS
type User struct {
	name string
	path string
	role *Role
	expire time.Time
	perms []string
	permsExpire time.Time
	data map[string]interface{}
	custom map[string]interface{}
}

func (user *User) Name() string {
	return user.name
}
func (user *User) Role() *Role {
	return user.role
}
func (user *User) Data() map[string]interface{} {
	return user.data
}
func (user *User) Custom() map[string]interface{} {
	return user.custom
}
func (user *User) Whoami() map[string]interface{} {
	return map[string]interface{}{
		"user": user.Name(),
		"role": user.Role().Name(),
		"data": user.Data(),
		"custom": user.Custom(),
	}
}

// Users routine (expires caches) - only executed if auth turned on
func (api *MqApi) usersAuthRoutine() {
	api.usersLock <- true
	go func(){
		for {
			<- api.usersLock
			now := time.Now()
			for up, u := range api.users {
				if now.After(u.expire) {
					delete(api.users, up)
				}
			}
			api.usersLock <- true
			time.Sleep(60 * time.Second)
		}
	}()
}

// Gets user, from cache or DB. Checks for correctness. Extracts user information.
func (api *MqApi) AuthUser(user string)  (*User, error){
	<- api.usersLock
	defer func() {
		api.usersLock <- true
	}()
	user = strings.ToLower(user)
	userSplit := strings.Split(user, "/")
	name := userSplit[len(userSplit)-1]
	u, userExists := api.users[user]
	now := time.Now()

	// User not cached
	if !userExists {
		u = &User{}
		u.path = user
		u.name = name // User name
		u.permsExpire = time.Unix(0,0) // Set the perms as expired	
	}

	// Refresh permissions
	if now.After(u.permsExpire) {
		// Perms
		res, err := api.MQSend(map[string]interface{}{
			"type": "permission_search",
			"data": map[string]interface{}{
				"directory": api.mqPath,
				"owner":     "self",
				"user":      user,
				"fields":    []string{"perms"},
			},
		})
		if err != nil {
			return nil, err
		}
		permList := res["data"].([]interface{})
		u.perms = []string{}
		for _, perm := range permList {
			perms := perm.(map[string]interface{})
			for _, p := range perms["perms"].([]interface{}) {
				if strings.HasPrefix(p.(string), api.mqPermsPrefix) {
					permStr := p.(string)[len(api.mqPermsPrefix):]
					u.perms = append(u.perms, permStr)
				}
			}
		}
		// Tags
		find, err := api.MQSend(map[string]interface{}{
			"type": "object_get_info",
			"data": map[string]interface{}{
				"path":   user,
				"fields": []string{"tags"},
			},
		})
		if err != nil {
			return nil, err
		}
		tags, ok := find["data"].(map[string]interface{})["tags"].(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("user <%s> misses needed tags", name)
		}
		// Role
		if api.hasRoles {
			role, ok := tags["role"].(string)
			if !ok {
				return nil, fmt.Errorf("user <%s> misses tag [role]", name)
			}
			if !api.HasRole(role) {
				return nil, fmt.Errorf("role <%s> not valid, must be one of %#v", role, api.RoleNames())
			}
			u.role = api.roles[role]
		}	
		// User data
		d, ok := tags["data"].(map[string]interface{})
		if !ok {
			d = map[string]interface{}{}
		}
		u.data = d
		// User custom data
		c, ok := tags["custom"].(map[string]interface{})
		if !ok {
			c = map[string]interface{}{}
		}
		u.custom = c
		// Expire the permissions
		u.permsExpire = now.Add(time.Duration(api.permsExpire) * time.Second)
	}

	// Refresh cache
	u.expire = now.Add(time.Duration(api.usersExpire) * time.Second)
	
	// Cache the user
	if !userExists {
		api.users[user] = u
	}
	return u, nil
}

// Expires user, next call will retrieve user info again
func (api *MqApi) ReloadUser(user *User) {
	<- api.usersLock
	delete(api.users, user.path)
	api.usersLock <- true
	api.AuthUser(user.path)
}


// ROLES
type Role struct {
	role string
	roles map[string]int
}

// Gets role name
func (role *Role) Name() string {
	return role.role
}

// Gets role names
func (api *MqApi) RoleNames() ([]string) {
	return api.roleNames
}
// Gets one api role
func (api *MqApi) Role(role string) *Role {
	r, ok := api.roles[role]
	if !ok {
		panic("Role "+role+" not valid.")
	}
	return r
}
// Checks if the role is a valid one
func (api *MqApi) HasRole(role string) bool {
	_, ok := api.roles[role]
	return ok
}

// Sets the role hierachy (lower to higher)
func (api *MqApi) SetUserRoles(roles []string) {
	if len(roles) != 0 {
		api.hasRoles = true
		api.roleNames = roles
		api.roles = map[string]*Role{}
		for i, r := range roles {
			role := &Role{r, map[string]int{r: 0}}
			for _, lr := range roles[:i] {
				role.roles[lr] = -1
			}
			for _, hr := range roles[i+1:] {
				role.roles[hr] = 1
			}
			api.roles[r] = role
		}
	} else {
		api.hasRoles = false
		api.roleNames = []string{}
		api.roles = map[string]*Role{}
	}
}

func (api *MqApi) lowerRoles(role string) map[string]bool {
	res := map[string]bool{}
	for r, n := range api.Role(role).roles {
		res[r] = n < 0
	}
	return res
}
func (api *MqApi) higherRoles(role string) map[string]bool {
	res := map[string]bool{}
	for r, n := range api.Role(role).roles {
		res[r] = n > 0
	}
	return res
}
func (api *MqApi) lowerEqRoles(role string) map[string]bool {
	res := map[string]bool{}
	for r, n := range api.Role(role).roles {
		res[r] = n <= 0
	}
	return res
}
func (api *MqApi) higherEqRoles(role string) map[string]bool {
	res := map[string]bool{}
	for r, n := range api.Role(role).roles {
		res[r] = n >= 0
	}
	return res
}
func (api *MqApi) Roles(rule string) map[string]bool {
	if cached, ok := api.rolesCheckCache[rule]; ok {
		return cached
	}
	var res map[string]bool
	if strings.HasPrefix(rule, ">=") {
		res = api.higherEqRoles(rule[2:])
	} else if strings.HasPrefix(rule, ">") {
		res = api.higherRoles(rule[1:])
	} else if strings.HasPrefix(rule, "<=") {
		res = api.lowerEqRoles(rule[2:])
	} else if strings.HasPrefix(rule, "<") {
		res = api.lowerRoles(rule[1:])
	} else if rule == "*" {
		res = map[string]bool{}
		for r, _ := range api.roles {
			res[r] = true
		}
	} else {
		res = map[string]bool{}
		for r, _ := range api.roles {
			res[r] = false
		}
		for _, role := range strings.Split(strings.Replace(rule, " ", "", -1), ",") {
			if _, ok := res[role]; ok {
				res[role] = true
			} else {
				panic("Role "+role+" not valid.")
			}
		}
	}
	api.rolesCheckCache[rule] = res
	return res
}
func (api *MqApi) AnyRoles(rules ...string) map[string]bool {
	res := map[string]bool{}
	for _, role := range api.RoleNames() {
		res[role] = false	
	}
	for _, rule := range rules {
		for role, present := range api.Roles(rule) {
			if !res[role] && present {
				res[role] = true
			}
		}
	}
	return res
}
func (api *MqApi) AllRoles(rules ...string) map[string]bool {
	res := map[string]bool{}
	for _, role := range api.RoleNames() {
		res[role] = true	
	}
	for _, rule := range rules {
		for role, present := range api.Roles(rule) {
			if res[role] && !present {
				res[role] = false
			}
		}
	}
	return res
}

// Checks if a role is in a group of roles
func (role *Role) In(roles map[string]bool) bool {
	return roles[role.Name()]
}
func formatRoles(roles map[string]bool) string {
	out := []string{}
	for r, ok := range roles {
		if ok {
			out = append(out, r)
		}
	}
	return strings.Join(out, ",")
}